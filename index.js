'use strict';

const resolver = require('resolve');
const path = require('path');

const aliases = {};

function retryResolve(attempt, potentialPaths, originalUrl, callback) {
  if (potentialPaths[attempt]) {
    let res;
    try {
      res = resolver.sync(potentialPaths[attempt], {extensions: '.scss'});
    } catch (e) {
      return retryResolve(attempt + 1, potentialPaths, originalUrl, callback);
    }
    return res;
  }
  else {
    throw new Error('Could not find ' + originalUrl);
  }
}

module.exports = function(url, file, done) {
  if (aliases[url]) {
    return done({file: aliases[url]});
  }
  if (url.indexOf('~') === 0) {
    url = url.substr(1, url.length);
    let potentialPaths = [];
    potentialPaths.push(url);
    let fileName = path.basename(url);
    let ext = path.extname(url);
    if (ext.length === 0) {
      ext = null;
    }
    fileName = fileName + (ext || '.scss');
    potentialPaths.push(path.join(path.dirname(url), '_' + fileName));
    potentialPaths.push(path.join(path.dirname(url), fileName));
    const result = retryResolve(0, potentialPaths, url);
    if (typeof result === 'string') {
      done({file: result});
    }

  }
  else {
    // if your module could not be found, just return the original url
    aliases[url] = url;
    return done({file: url});
  }
};
